FROM python:3.8-slim

WORKDIR /usr/src/app

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
    curl \
    cron \
    nginx \
    apache2-utils \
    libssl-dev \
    libxml2-dev \
    locales \
    build-essential && \
    pip install --no-cache-dir https://github.com/kikihakiem/chaperone/archive/master.zip && \
    pip install --no-cache-dir scrapyd scrapyd-client && \
    mkdir /etc/chaperone.d && \
    apt-get remove -y \
    libssl-dev \
    libxml2-dev \
    build-essential && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV TZ='Asia/Jakarta'
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    sed -i -e 's/# \(en_US\.UTF-8 .*\)/\1/' /etc/locale.gen && \
    locale-gen && \
    update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8

VOLUME /scrapyd

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ARG COMMIT_REF
ARG COMMIT_HASH

COPY . .

ADD scrapyd.conf /etc/scrapyd/scrapyd.conf
ADD nginx.conf /etc/nginx/sites-enabled/default
ADD chaperone.conf /etc/chaperone.d/chaperone.conf

RUN mkdir -p /var/www/app && \
    ./version.py "$(scrapy list | paste -sd ',' -)" "$(date)" > /var/www/app/version.json

EXPOSE 80

ENTRYPOINT ["/usr/local/bin/chaperone"]
