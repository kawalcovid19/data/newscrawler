#!/usr/bin/env python3

import json
import sys
import os

data = {
  'spiders': sys.argv[1].split(','),
  'timestamp': sys.argv[2],
  'ref': os.getenv('COMMIT_REF'),
  'hash': os.getenv('COMMIT_HASH')
}

print(json.dumps(data, indent=4))
