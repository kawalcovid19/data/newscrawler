# KawalCOVID-19 News Crawler

Collect corona related news from Indonesian media(s).

## Setup

Clone this repository:

    git clone https://gitlab.com/kawalcovid19/kawalcovid19-newscrawler.git
    cd kawalcovid19-newscrawler

Create new python virtual environment:

    python3 -m venv venv

Activate the virtual environment:

    # Unix
    source venv/bin/activate

    # Windows (Powershell)
    .\venv\Scripts\activate.bat

Install the dependencies:

    (venv) % pip install -r requirements.txt

If you are using VSCode: the linter, formatter & static type checker will be
enabled by default.

## Run

To run the available spiders, use the following command:

    BASICAI_AZURE_BLOB_CONTAINER="basicai-dataset" \
    BASICAI_AZURE_BLOB_CONN_STRING="UPDATE_HERE" \
    scrapy crawl $SPIDER_NAME


## Resources

- [Scrapy 2.0 documentation](https://docs.scrapy.org/en/latest/)
- [Evaluate and validate XPath/CSS selectors in Chrome Developer Tools](https://yizeng.me/2014/03/23/evaluate-and-validate-xpath-css-selectors-in-chrome-developer-tools/)
- [Xpath cheatsheet](https://devhints.io/xpath#axes)