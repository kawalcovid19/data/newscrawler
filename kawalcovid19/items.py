import scrapy
import json


class NewsItem(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    content = scrapy.Field()
    author = scrapy.Field()
    published_at = scrapy.Field()
    news_site = scrapy.Field()

    def to_json(self) -> str:
        item = {
            "url": self["url"][0],
            "title": self["title"][0],
            "content": self["content"][0],
            "author": self["author"][0],
            "published_at": self["published_at"][0],
            "news_site": self["news_site"][0]
        }
        return json.dumps(item)
