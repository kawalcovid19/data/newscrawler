from ..pipelines import BasePipelineTxtWriter, BasePipelineFilter
from ..items import NewsItem
from unittest.mock import Mock
from scrapy.exceptions import DropItem
import json
import pytest

TEXT_CONTAIN_CORONA_WORDS = 'corona words words words'
TEXT_CONTAIN_CORONA_WORDS1 = 'words COVID words'
TEXT_CONTAIN_FATAL_WORDS = 'words words KoRBan!! meninggal. words.'
TEXT_CONTAIN_FATAL_WORDS1 = 'words DIMAKAMKAN,,, MENINGGAL.//// words...'


def _create_news_item_mock_by_dict(data: dict) -> NewsItem:
    news_item = Mock()
    news_item.to_json = Mock()
    news_item.to_json.return_value = json.dumps(data)
    return news_item


def test_BasePipelineTxtWriterbuild_data_txt_success():
    base_pipeline = BasePipelineTxtWriter()
    news_item = _create_news_item_mock_by_dict({
        'title': 'TITLE',
        'content': 'CONTENT',
        'published_at': '2020-01-01',
        'url': 'URL',
        'author': 'AUTHOR',
        'news_site': 'NEWS_SITE'
    })
    assert """TITLE
----------
2020-01-01
----------
URL
----------
AUTHOR
----------
NEWS_SITE
----------
CONTENT""" == base_pipeline.build_data_txt(news_item)


def test_BasePipelineFilter():
    pipeline_filter = BasePipelineFilter()
    spider_mock = Mock()

    news_item = _create_news_item_mock_by_dict({
        'title': TEXT_CONTAIN_CORONA_WORDS + ' ' + TEXT_CONTAIN_FATAL_WORDS,
        'content': TEXT_CONTAIN_FATAL_WORDS
    })
    pipeline_filter.process_item(news_item, spider_mock)

    news_item = _create_news_item_mock_by_dict({
        'title': TEXT_CONTAIN_FATAL_WORDS1 + ' ' + TEXT_CONTAIN_CORONA_WORDS1,
        'content': TEXT_CONTAIN_CORONA_WORDS1
    })
    pipeline_filter.process_item(news_item, spider_mock)


def test_BasePipelineFilter_raise_exception():
    pipeline_filter = BasePipelineFilter()
    spider_mock = Mock()

    # have corona but not fatality news
    news_item = _create_news_item_mock_by_dict({
        'title': TEXT_CONTAIN_CORONA_WORDS,
        'content': 'content content Content'
    })
    pytest.raises(DropItem, pipeline_filter.process_item, news_item, spider_mock)

    # have fatality but not corona news
    news_item = _create_news_item_mock_by_dict({
        'title': TEXT_CONTAIN_FATAL_WORDS,
        'content': 'content content content'
    })
    pytest.raises(DropItem, pipeline_filter.process_item, news_item, spider_mock)

    # don't have both
    news_item = _create_news_item_mock_by_dict({
        'title': 'title title ',
        'content': 'content content Content'
    })
    pytest.raises(DropItem, pipeline_filter.process_item, news_item, spider_mock)
