# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import re
import os
import json
import time
import requests

from io import BytesIO
from datetime import datetime
from azure.storage.blob import ContainerClient
from azure.core.exceptions import ResourceExistsError
from scrapy.exceptions import DropItem
from slugify import slugify
from apiclient import discovery
from google.oauth2 import service_account

from kawalcovid19.items import NewsItem
from kawalcovid19.spiders.base import BaseSpider

char_limit = 255
GSHEET_ID = '1hdmZgCvRukGYXLv4tV77tfXI1G7gNCBZCOXYx-Mn53w'
GSHEET_RANGE = 'Sheet1!A:B'
GSHEET_HEADERS = ['url', 'title']


class BasePipelineTxtWriter(object):

    def __init__(self):
        self.separator = "\n----------\n"
        self.field_priority = ["title", "published_at", "url", "author", "news_site", "content"]

    def build_data_txt(self, item: NewsItem) -> str:
        data = json.loads(item.to_json())
        content = self.separator.join([data[field] for field in self.field_priority])
        return content


class BasePipelineFilter(object):

    def __init__(self):
        delimiter = ','
        self.words_to_filter = set(os.getenv("NEWS_FILTER_WORDS_FATALITY",
                                             "meninggal,korban,wafat")
                                     .split(delimiter))
        self.must_have_words = set(os.getenv("NEWS_FILTER_WORDS_COVID19",
                                             "corona,covid,covid19")
                                     .split(delimiter))
        self.fields_to_filter = set(os.getenv("NEWS_FILTER_FIELDS",
                                              "title")
                                      .split(delimiter))

    def _filter_word(self, item: NewsItem, list_words, list_fields) -> bool:
        data = json.loads(item.to_json())

        for field in list_fields:
            for word in re.split(r"\W+", data[field]):
                if word.lower() in list_words:
                    return True

        return False

    def _is_corona_news(self, item: NewsItem) -> bool:
        return self._filter_word(item, self.must_have_words, self.fields_to_filter)

    def _is_fatal_news(self, item: NewsItem) -> bool:
        return self._filter_word(item, self.words_to_filter, self.fields_to_filter)

    def process_item(self, item: NewsItem, spider: BaseSpider) -> NewsItem:
        if not self._is_corona_news(item) or not self._is_fatal_news(item):
            raise DropItem("Non fatality news!")

        return item


class BaseAzureStoragePipeline(object):

    def upload_blob(self, container: ContainerClient, blob: BytesIO, file_name: str) -> None:
        try:
            container.upload_blob(
                file_name, blob
            )
        except ResourceExistsError as e:
            raise DropItem(e)


# Drop the item if the required fields are not exists
class NewsValidationPipeline(object):
    @staticmethod
    def process_item(item: NewsItem, _spider: BaseSpider) -> NewsItem:
        title = item.get("title", "title_not_set")
        if title == "title_not_set":
            err_msg = "Missing title in: %s" % item.get("url")
            raise DropItem(err_msg)

        content = item.get("content", "content_not_set")
        if content == "content_not_set":
            err_msg = "Missing content in: %s" % item.get("url")
            raise DropItem(err_msg)

        published_at = item.get("published_at", "published_at_not_set")
        if published_at == "published_at_not_set":
            err_msg = "Missing published_at in: %s" % item.get("url")
            raise DropItem(err_msg)

        # Pass item to the next pipeline, if any
        return item


# Write every item received from spider to their own corresponding TXT file
class AzureBlobStorageBasicAITXTWriterPipeline(BaseAzureStoragePipeline, BasePipelineTxtWriter):

    def __init__(self):
        super().__init__()

    def process_item(self, item: NewsItem, spider: BaseSpider) -> NewsItem:
        # Create the blob first
        file_id = "{} {}".format(spider.name, item.get("title")[0])
        file_name = slugify(file_id)[:char_limit] + ".txt"
        content = self.build_data_txt(item)

        with BytesIO() as blob:
            blob.write(content.encode())
            # Seek back to 0, so when the blob container upload the content
            # it read from the start
            blob.seek(0)
            self.upload_blob(spider.azure_basicai_container, blob, file_name)
        return item


# Write every item received from the spider to their own corresponding
# JSON file
class AzureBlobStorageJsonWriterPipeline(BaseAzureStoragePipeline):

    def process_item(self, item: NewsItem, spider: BaseSpider) -> NewsItem:
        # Create the blob first
        file_id = "{} {}".format(spider.name, item.get("title")[0])
        file_name = slugify(file_id)[:char_limit] + ".json"
        with BytesIO() as blob:
            blob.write(item.to_json().encode())
            blob.write("\n".encode())
            # Seek back to 0, so when the bldob container upload the content
            # it read from the start
            blob.seek(0)
            self.upload_blob(spider.azure_json_container, blob, file_name)

        return item


class GsheetAppenderPipeline(object):
    def __init__(self):
        self.scopes = ['https://www.googleapis.com/auth/drive',
                       'https://www.googleapis.com/auth/drive.file',
                       'https://www.googleapis.com/auth/spreadsheets']
        self.spreadsheet_id = GSHEET_ID
        self.range_name = GSHEET_RANGE

    def open_spider(self, spider):
        self.key_dict = json.loads(os.environ['GSHEET_CRED'])
        credentials = service_account.Credentials.from_service_account_info(
                self.key_dict, scopes=self.scopes)
        service = discovery.build('sheets', 'v4', credentials=credentials)
        self.gsheet_service = service

    def process_item(self, item: NewsItem, spider: BaseSpider) -> NewsItem:
        list_news_url = [[item.get(x)[0] for x in GSHEET_HEADERS]]
        data = {
            "majorDimension": "ROWS",
            "values": list_news_url
        }

        self.gsheet_service.spreadsheets().values().append(
            spreadsheetId=self.spreadsheet_id,
            body=data,
            range=self.range_name,
            valueInputOption='USER_ENTERED').execute()

        return item


class BasicAISubmitterPipeline(BasePipelineTxtWriter):

    def __init__(self):
        self.session = requests.Session()
        self._build_token()

        super().__init__()

    def _build_token(self):
        self.session.get("https://passport.saas.basic.ai/login")

        username = os.getenv("BASICAI_USERNAME")
        password = os.getenv("BASICAI_PASSWORD")
        body = {
            "username": username,
            "password": password,
            "device_name": "Linux x86_64",
            "device_number": 111,
            "app_key": "pc-passport",
            "app_version": "1.0.0",
            "language": 1
        }

        response = self.session.post("https://api.saas.basic.ai/site/login", data=body)
        data = json.loads(response.content)["data"]
        self.token = data["access_token"]
        self.id = data["id"]

    def _prepare_upload(self):
        body = {
            "access_token": self.token,
            "data_manage_id": self.data_manage_id
        }

        response = self.session.post("https://api.saas.basic.ai/datamanage/detail", data=body)
        data = json.loads(response.content)["data"]

        self.deploy_path = data["upload_path"]
        self.upload_type = data["upload_type"]
        self.file_operate = data["file_operate"]
        self.file_type = data["file_type"]

    def _submit_to_basicai(self, file_id):
        files = {
            "file": open(file_id, 'rb')
        }

        body = {
            "access_token": self.token,
            "deploy_path": self.deploy_path
        }

        self.session.post("https://api.saas.basic.ai/site/upload-private-file", files=files, data=body)

    def open_spider(self, spider):
        body = {
            "access_token": self.token,
            "file_type": 2
        }

        response = self.session.post("https://api.saas.basic.ai/datamanage/create", data=body)
        data = json.loads(response.content)["data"]
        self.data_manage_id = data["id"]

        self._prepare_upload()

    def close_spider(self, spider):
        # waiting file to be uploaded
        time.sleep(3)

        date = datetime.now().strftime("%m-%d-%Y %H:%M:%S")
        name = "{}-{}".format(spider.name, date)
        body = {
            "access_token": self.token,
            "data_manage_id": self.data_manage_id,
            "name": name,
            "upload_type": self.upload_type,
            "file_operate": self.file_operate,
            "file_type": self.file_type,
            "merge_count": None
        }

        max_retry = 3
        while max_retry > 0:
            response = self.session.post("https://api.saas.basic.ai/datamanage/submit", data=body)
            response = json.loads(response.content)

            # if data already exist it will return error with name as value
            if response["error"] == "" or response["error"] == "name":
                max_retry = 0
            else:
                max_retry -= 1
                time.sleep(5)

        print("Success")

    def process_item(self, item: NewsItem, spider: BaseSpider) -> None:
        # Create the blob first

        content = self.build_data_txt(item)

        file_id = "{} {}.txt".format(spider.name, item.get("title")[0])
        file = open(file_id, "wb+")
        file.write(content.encode())
        file.close()

        self._submit_to_basicai(file_id)
        os.remove(file_id)

        return item
