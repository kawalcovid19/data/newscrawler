import locale

from datetime import datetime
from scrapy import Request
from scrapy.exceptions import CloseSpider

from kawalcovid19.spiders.base import BaseSpider


class TirtoSpider(BaseSpider):
    name = "tirto"
    base_url = "https://tirto.id"
    allowed_domains = ["tirto.id"]
    start_urls = ["https://tirto.id/search?q=corona"]
    page = 0

    def parse(self, response):
        self.page += 1
        if self.page > 5:
            raise CloseSpider('First 5 pages, done!')

        links = response.xpath(
            '//div[@class="col-md-4 mb-4 news-list-fade"]//a/@href'
        ).getall()

        for link in links:
            url = "{}/{}".format(self.base_url, link[2:])
            yield Request(url=url, callback=self.parse_article)

        next_pages = response.xpath('//li[@class="pagination-item"]/a/@href').getall()

        for next_page in next_pages:
            url = "{}{}".format(self.base_url, next_page)
            yield Request(url=url, callback=self.parse)

    def parse_article(self, response):
        title = response.xpath(
            '//h1[@class="news-detail-title text-center animated zoomInUp my-3"]/text()'
        ).get()

        author, date_str = (
            response.xpath('//span[@class="detail-date mt-1 text-left"]/text()')
            .get()[5:]
            .split("-")
        )

        author = author.strip()

        locale.setlocale(locale.LC_ALL, "id_ID.UTF-8")
        date = datetime.strptime(date_str.strip(), "%d %B %Y")

        contents = response.xpath(
            '//div[@class="content-text-editor"][2]//text()[not(ancestor::div[@class="baca-holder bold"])]'
        ).getall()
        content = " ".join(
            [content.strip() for content in contents if len(content.strip()) > 0]
        )

        return self.to_item_loader(response, title, content, author, date)
