from typing import Iterable

from scrapy import Request
from scrapy.exceptions import CloseSpider
from scrapy.http import Response
from scrapy.loader import ItemLoader

from kawalcovid19.items import NewsItem
from kawalcovid19.spiders.base import BaseSpider


class KompasSpider(BaseSpider):
    name = "kompas"
    allowed_domains = ["kompas.com"]
    start_urls = [
        "https://www.kompas.com/topik-pilihan/list/6178/wabah.virus.corona"
    ]

    def parse(self, response: Response) -> Iterable:
        # Get all article links on the page
        article_links_selector = response.css("a.article__link")
        if not article_links_selector:
            # Close the spider if there is no article links on the page
            raise CloseSpider("article_links not found")

        # For each article link in the page, follow the link then parse the
        # article
        for article_link_selector in article_links_selector:
            article_link = article_link_selector.attrib["href"]
            yield Request(
                url=article_link + "?page=all", callback=self.parse_article
            )

    def parse_article(self, response: Response) -> NewsItem:
        self.logger.info("parse_news: %s" % response)
        # Setup Item loader
        article = ItemLoader(item=NewsItem(), response=response)

        # Add news site
        article.add_value("news_site", self.name)

        # Add article's URL
        article.add_value("url", response.url)

        # Add article's title
        article_title = response.xpath(
            "//h1[@class='read__title']/text()"
        ).get()
        article.add_value("title", article_title)

        # Add article's content
        # Select all paragraphs in the main content, except paragraph
        # that contains "Baca juga"
        paragraph_selectors = response.xpath(
            """
            //div[@class='read__content']
                //p[not(contains(descendant-or-self::text(), 'Baca juga'))]
            """
        )
        content = ""
        for paragraph_selector in paragraph_selectors:
            # Extract all raw text from the paragraph
            raw_texts = paragraph_selector.xpath(
                "./descendant-or-self::text()"
            ).getall()
            # Join the text as one paragraph
            paragraph = "".join(raw_texts).strip()
            # Concat paragraph to a content
            content += paragraph + "\n"
        article.add_value("content", content)

        # Add article's author
        author = response.xpath("//div[@id='penulis']/a/text()").get()
        article.add_value("author", author)

        # Add article's publish timestamp
        raw_published_at = response.xpath(
            "//div[@class='read__time']/text()"
        ).get()
        # TODO(bay): Convert raw_published_at string to datetime
        article.add_value("published_at", raw_published_at)

        # Load the scraped item to the pipelines
        return article.load_item()
