#!/usr/bin/env bash

set -e

# x 9,18 * * * curl http://127.0.0.1:6800/schedule.json -d project=kawalcovid19 -d spider=CNN >/dev/null 2>&1
# where x = spider index * 5 (currently CNN=5, detik=10 and so on)
scrapy list | \
  sed -e "s/\(.*\)/9,18,23 \* \* \* curl http:\/\/127.0.0.1:6800\/schedule.json -d project=kawalcovid19 -d spider=\1 > \/proc\/1\/fd\/1 2>\/proc\/1\/fd\/2/" | \
  sed '/.*/!d;=' | \
  sed 'N;s/\n/ /' | \
  awk '{ $1 *= 5 } 1' > /etc/cron.d/spider.cron
touch /var/log/cron.log
chmod 0644 /etc/cron.d/spider.cron
crontab /etc/cron.d/spider.cron
